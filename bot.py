
from handlers.table import register_handlers_table
import logging
from aiogram import Bot, Dispatcher, executor, types, asyncio
from aiogram.types import BotCommand
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import FSMContext
from os import getenv
from sys import exit
import configparser
from random import randint

logger = logging.getLogger(__name__)

# Регистрация команд, отображаемых в интерфейсе Telegram
async def set_commands(bot_var: Bot):
    commands = [
        BotCommand(command="/start", description="Старт!"),
        BotCommand(command="/input_google_table", description="Передать ссылку на расписание"),
        BotCommand(command="/cancel", description="Начать сначала")
    ]
    await bot_var.set_my_commands(commands)


async def main():
    # Настройка логирования в stdout
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
    )
    logger.error("Starting bot")

    # Запуск бота
    config = configparser.ConfigParser()
    config.read('config/bot.ini')
    # Объявление и инициализация объектов бота и диспетчера
    bot = Bot(token=config['DEFAULT']['BOT_TOKEN'], parse_mode=types.ParseMode.HTML)
    dp = Dispatcher(bot, storage=MemoryStorage())

    register_handlers_table(dp)

    @dp.message_handler()
    async def any_text_message(message: types.Message):
        await message.reply("Такую команду я не знаю. Запутались? Начните с команды /start")
    # Установка команд бота
    await set_commands(bot)

    await dp.start_polling()


if __name__ == "__main__":
    asyncio.run(main())


