from __future__ import print_function

import os.path
import datetime

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1aGuISe012Qo-rmlueqh0BhdVCqvJe9xxCaBDM_4h324'
SAMPLE_RANGE_NAME = 'A1:J250'


def get_google_docs():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('config/g-token.json'):
        creds = Credentials.from_authorized_user_file('config/g-token.json', SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'config/g-credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('config/g-token.json', 'w') as token:
            token.write(creds.to_json())

    try:
        service = build('sheets', 'v4', credentials=creds)

        # Call the Sheets API
        return service

    except HttpError as err:
        print(err)


def get_sheet_lists(g_doc_id):
    service = get_google_docs()
    sheets = service.spreadsheets().get(spreadsheetId=g_doc_id).execute()
    sheets = sheets.get('sheets')
    res = []
    for sheet in sheets:
        title = sheet.get("properties", {}).get("title", "Sheet Name")
        sheet_id = sheet.get("properties", {}).get("sheetId", 0)
        if sheet_id == 0:
            continue
        res.append(title)
    return res


def get_sheet_data(g_doc_id, sheet_name):
    service = get_google_docs()
    range = sheet_name + "!" + SAMPLE_RANGE_NAME
    data = service.spreadsheets().values().get(spreadsheetId=g_doc_id, range=range).execute()
    return data['values']


def get_timetable_from_sheet_data(sheet_data=[]):
    days = []
    header_cnt = 0
    for row in sheet_data:
        if len(row) == 0:
            header_cnt += 1
            continue
        if row[0].lower() != 'день':
            header_cnt += 1
        else:
            break
    for i in range(0, header_cnt + 1):
        sheet_data.pop(0)
    for row in sheet_data:
        if row[00] != '':
            days.append([])
        for cell in row:
            if cell != '':
                days[-1].append(cell)
    days_ahead = []
    days.pop(-1)
    days.pop(-1)
    for day in days:
        if len(days_ahead) > 2:
            break
        n_symbol = day[00].find("\n")
        str_date = day[00][n_symbol + 1:]
        date = datetime.datetime.strptime(str_date, '%d.%m.%Y')
        if date > datetime.datetime.now() - datetime.timedelta(days=1):
            days_ahead.append(day)
    messages = []
    for day in days_ahead:
        result = "<b>" + day.pop(00) + "</b>\n"
        i = 0
        for cell in day:
            # Пропустили номер пары
            if i == 0:
                i += 1
                continue
            if i == 1:
                result += "<b>Время</b>: " + cell + "\n"
                i += 1
                continue
            if i == 2:
                result += "<b>Предмет</b>: " + cell + "\n"
                i += 1
                continue
            if i == 3:
                result += "<b>Место</b>: " + cell.replace('\n', ' ') + "\n\n"
                i = 0
        messages.append(result)
    if len(messages) == 0:
        messages.append(
            "Похоже, занятия уже прошли или на будущие дни информация еще не поступила. Возвращайтесь в следующий раз!")
    return messages
