import asyncio

from aiogram import Dispatcher, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from controllers.spreadsheet import get_sheet_lists, get_sheet_data, get_timetable_from_sheet_data


class gSheetView(StatesGroup):
    session_start = State()
    waiting_for_g_table = State()
    waiting_for_group = State()
    waiting_for_restart = State()


async def check_google_list(message: types.Message, state: FSMContext):
    curr_state = await state.get_data()
    group_list = curr_state['group_list']
    g_doc_id = curr_state['g_doc_id']
    if message.text not in group_list:
        await message.answer("Такой группы нет, выбери свою с помощью клавиатуры")
        return
    await message.reply("Загружаю расписание группы <b>" + message.text + "</b> на следующие три дня")
    await asyncio.sleep(1.5)
    await state.update_data(chooses_group=message.text)
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(types.InlineKeyboardButton(text="Начать сначала", callback_data="restart"))
    keyboard.add(types.InlineKeyboardButton(text="Выбрать другую группу", callback_data="go_to_chose_group"))
    sheet_data = get_sheet_data(g_doc_id, message.text)
    messages = get_timetable_from_sheet_data(sheet_data)
    last_message = messages.pop(-1)
    for msg in messages:
        await message.answer(msg)
    await message.answer(last_message, reply_markup=keyboard)
    await gSheetView.waiting_for_restart.set()

async def check_google_table(message: types.Message, state: FSMContext):
    curr_state = await state.get_data()
    if curr_state.get('g_doc_id') is None:
        validated_link = message.text
        if validated_link[0:39] != 'https://docs.google.com/spreadsheets/d/':
            await message.answer("Попробуй еще раз, я знаю ссылки, которые начинаются на <b>https://docs.google.com/spreadsheets/d/</b>. Или начни сначала /start")
            return
        validated_link = validated_link[39:]
        slash_symbol = validated_link.find("/")
        validated_link = validated_link[0:slash_symbol]
        await state.update_data(g_doc_id=validated_link)
    else:
        validated_link = curr_state['g_doc_id']
    group_list = get_sheet_lists(validated_link)
    await state.update_data(group_list=group_list)
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    for group in group_list:
        keyboard.add(group)
    await message.answer("Выберите группу:", reply_markup=keyboard)
    await gSheetView.waiting_for_group.set()


async def check_again_google_table(call: types.CallbackQuery, state: FSMContext):
    await check_google_table(call.message, state)


async def restart_bot(call: types.CallbackQuery, state: FSMContext):
    await state.finish()
    await cmd_start(call.message)


async def input_google_table(call: types.CallbackQuery):
    await call.message.answer("Готов принять ссылку на вашу таблицу с расписанием!")
    await gSheetView.waiting_for_g_table.set()


async def cmd_start(message: types.Message):
    get_sheet_lists('1aGuISe012Qo-rmlueqh0BhdVCqvJe9xxCaBDM_4h324')
    await message.answer("<b>Привет!</b>")
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(types.InlineKeyboardButton(text="Отлично, готов отправить ссылку", callback_data="input_google_table"))
    await message.answer(("Я могу отправить вам расписание на ближайшие три дня.\n" +
                         "Просто отправьте ссылку на Google-таблицу, а я выдам вам ваше расписание в удобном виде"), reply_markup=keyboard)
    await gSheetView.session_start.set()


async def bad_input_google_table(message: types.Message):
    await message.answer("Нажмите на кнопку выше")


def register_handlers_table(dp: Dispatcher):
    dp.register_message_handler(cmd_start, commands="start", state="*")
    dp.register_message_handler(bad_input_google_table, state=gSheetView.session_start)
    dp.register_callback_query_handler(input_google_table, text="input_google_table", state=gSheetView.session_start)
    dp.register_message_handler(check_google_table, state=gSheetView.waiting_for_g_table)
    dp.register_message_handler(check_google_list, state=gSheetView.waiting_for_group)
    dp.register_callback_query_handler(check_again_google_table, text="go_to_chose_group",
                                       state=gSheetView.waiting_for_restart)
    dp.register_callback_query_handler(restart_bot, text="restart",
                                       state=gSheetView.waiting_for_restart)


